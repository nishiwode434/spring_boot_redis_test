package com.wjs.mapper;

import com.wjs.model.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * Created by 18435 on 2017/10/13.
 */

@Mapper
public interface ProductMapper {
    Product select(@Param("id") Integer id);
    void update(@Param("id") Integer id, @Param("price") Integer price);
    void insertProduct(@Param("product") Product product);
}
