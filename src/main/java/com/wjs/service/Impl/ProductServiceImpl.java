package com.wjs.service.Impl;

import com.wjs.mapper.ProductMapper;
import com.wjs.model.Product;
import com.wjs.service.ProductSercvice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * Created by 18435 on 2017/10/14.
 *
 * @Cacheable:缓存id到redis-cache中
 * @CachePut：缓存新增的或者更新的数据到缓存
 * @CacheEvit：从缓存redis-cache中删除key为id的缓存
 *
 * 代码部分是使用redis做数据库（方便存取）
 */

@Service
public class ProductServiceImpl implements ProductSercvice {
    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private StringRedisTemplate template;

    @Autowired
    private ProductMapper productMapper;

    @Override
    @Cacheable(value = "redis-cache", key = "#id.toString()")
    public Product findProductById(Integer id) {
        Product product;
////         从缓存中获取城市信息
//        String key = id.toString();
//
//        ValueOperations<String, Product> valueOperations = redisTemplate.opsForValue();
//        boolean hasKey = redisTemplate.hasKey(key);
//        if(hasKey) {
//            product = valueOperations.get(key);
//            logger.info("CityServiceImpl.findCityById() : 从缓存中获取了城市 >> " + product.toString());
//            return product;
//        }
        product = productMapper.select(id);
//        if (null == product) {
//            throw new IllegalArgumentException();
//        }
//        valueOperations.set(key, product, 10, TimeUnit.SECONDS);
//        Product product1 = valueOperations.get(key);
//
//        logger.debug("产品信息：{}", product1.toString());
        return product;
    }

    @Override
    public void setProduct(String key) {
        ValueOperations operations = template.opsForValue();
        operations.set(key, "zz");
        System.out.print(operations.get(key));
    }
}
