package com.wjs.controller;


import com.wjs.mapper.ProductMapper;
import com.wjs.model.Product;
import com.wjs.service.ProductSercvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 18435 on 2017/10/13.
 */


@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    ProductSercvice productSercvice;

    @GetMapping("/{id}")
    public ResponseEntity getProductInfo(
            @PathVariable("id") Integer productId) {
        return ResponseEntity.ok(productSercvice.findProductById(productId));
    }

//    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
//    public ResponseEntity updateProductInfo(
//            @PathVariable("id") Integer productId,
//            @RequestBody Integer price) {
//        productMapper.update(productId, price);
//        return null;
//    }

    @RequestMapping(value = "add/product", method = RequestMethod.POST)
    public ResponseEntity addProduct(
            @RequestBody Product product
            ) {
        if (null == product)
            throw new IllegalArgumentException("核对参数");
        productMapper.insertProduct(product);
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @RequestMapping(value = "set/test", method = RequestMethod.GET)
    public ResponseEntity setTest(
            @RequestParam String key
    ) {
        productSercvice.setProduct(key);
        return ResponseEntity.ok(HttpStatus.OK);
    }

}
